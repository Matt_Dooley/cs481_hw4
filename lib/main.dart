import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    Widget americanSection = Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildRaysButtonRow(),
          _buildBlueJaysButtonRow(),
          _buildIndiansButtonRow(),
          _buildYankeesButtonRow(),
          _buildTwinsButtonRow(),
          _buildAstrosButtonRow(),
          _buildAthleticsButtonRow(),
          _buildWhiteSoxButtonRow(),
        ],
      ),
    );

    Widget nationalSection = Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildDodgersButtonRow(),
          _buildBrewersButtonRow(),
          _buildPadresButtonRow(),
          _buildCardinalsButtonRow(),
          _buildCubsButtonRow(),
          _buildMarlinsButtonRow(),
          _buildBravesButtonRow(),
          _buildRedsButtonRow(),
        ],
      ),
    );

    Widget bodySection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          americanSection,
          _buildCenterColumn(),
          nationalSection,
        ],
      ),
    );

    return MaterialApp(
      title: 'CS481 HW4',
      home: Scaffold(
        appBar: AppBar(
          title: Text('CS481 HW4'),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Image.asset('images/Postseason.png'),
            bodySection
          ],
        ),
      ),
    );
  }

  Column _buildCenterColumn() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ConstrainedBox(
          constraints: BoxConstraints(minWidth: 100, maxWidth: 200, minHeight: 300, maxHeight: 400),
          child: Image.asset('images/WorldSeries.png'),
          // child: Image.asset('images/WorldSeries.png'),
        ),
      ],
    );
  }

  Row _buildPadresButtonRow() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
          icon: (Image.asset('images/Padres.png')),
          color: Colors.blue[500],
          onPressed: _togglePadres,
        ),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            '4',
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: Colors.blue[500],
            ),
          ),
        ),
      ],
    );
  }

  Row _buildAstrosButtonRow() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
          icon: (Image.asset('images/Astros.png')),
          color: Colors.blue[500],
          onPressed: _toggleAstros,
        ),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            '6',
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: Colors.blue[500],
            ),
          ),
        ),
      ],
    );
  }

  Row _buildAthleticsButtonRow() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
          icon: (Image.asset('images/Athletics.png')),
          color: Colors.blue[500],
          onPressed: _toggleAthletics,
        ),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            '2',
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: Colors.blue[500],
            ),
          ),
        ),
      ],
    );
  }

  Row _buildBlueJaysButtonRow() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
          icon: (Image.asset('images/BlueJays.png')),
          color: Colors.blue[500],
          onPressed: _toggleBlueJays,
        ),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            '8',
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: Colors.blue[500],
            ),
          ),
        ),
      ],
    );
  }

  Row _buildBravesButtonRow() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
          icon: (Image.asset('images/Braves.png')),
          color: Colors.blue[500],
          onPressed: _toggleBraves,
        ),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            '2',
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: Colors.blue[500],
            ),
          ),
        ),
      ],
    );
  }

  Row _buildBrewersButtonRow() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
          icon: (Image.asset('images/Brewers.png')),
          color: Colors.blue[500],
          onPressed: _toggleBrewers,
        ),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            '8',
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: Colors.blue[500],
            ),
          ),
        ),
      ],
    );
  }

  Row _buildCardinalsButtonRow() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
          icon: (Image.asset('images/Cardinals.png')),
          color: Colors.blue[500],
          onPressed: _toggleCardinals,
        ),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            '5',
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: Colors.blue[500],
            ),
          ),
        ),
      ],
    );
  }

  Row _buildCubsButtonRow() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
          icon: (Image.asset('images/Cubs.png')),
          color: Colors.blue[500],
          onPressed: _toggleCubs,
        ),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            '3',
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: Colors.blue[500],
            ),
          ),
        ),
      ],
    );
  }

  Row _buildDodgersButtonRow() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
          icon: (Image.asset('images/Dodgers.png')),
          color: Colors.blue[500],
          onPressed: _toggleDodgers,
        ),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            '1',
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: Colors.blue[500],
            ),
          ),
        ),
      ],
    );
  }

  Row _buildIndiansButtonRow() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
          icon: (Image.asset('images/Indians.png')),
          color: Colors.blue[500],
          onPressed: _toggleIndians,
        ),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            '4',
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: Colors.blue[500],
            ),
          ),
        ),
      ],
    );
  }

  Row _buildMarlinsButtonRow() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
          icon: (Image.asset('images/Marlins.png')),
          color: Colors.blue[500],
          onPressed: _toggleMarlins,
        ),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            '6',
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: Colors.blue[500],
            ),
          ),
        ),
      ],
    );
  }

  Row _buildRaysButtonRow() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
          icon: (Image.asset('images/Rays.png')),
          color: Colors.blue[500],
          onPressed: _toggleRays,
        ),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            '1',
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: Colors.blue[500],
            ),
          ),
        ),
      ],
    );
  }

  Row _buildRedsButtonRow() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
          icon: (Image.asset('images/Reds.png')),
          color: Colors.blue[500],
          onPressed: _toggleReds,
        ),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            '7',
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: Colors.blue[500],
            ),
          ),
        ),
      ],
    );
  }

  Row _buildTwinsButtonRow() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
          icon: (Image.asset('images/Twins.png')),
          color: Colors.blue[500],
          onPressed: _toggleTwins,
        ),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            '3',
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: Colors.blue[500],
            ),
          ),
        ),
      ],
    );
  }

  Row _buildWhiteSoxButtonRow() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
          icon: (Image.asset('images/WhiteSox.png')),
          color: Colors.blue[500],
          onPressed: _toggleWhiteSox,
        ),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            '7',
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: Colors.blue[500],
            ),
          ),
        ),
      ],
    );
  }

  Row _buildYankeesButtonRow() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
          icon: (Image.asset('images/Yankees.png')),
          color: Colors.blue[500],
          onPressed: _toggleYankees,
        ),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            '5',
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: Colors.blue[500],
            ),
          ),
        ),
      ],
    );
  }

  void _togglePadres() {
    setState(() {
      runApp(PadresApp());
    });
  }

  void _toggleAstros() {
    setState(() {
      runApp(AstrosApp());
    });
  }

  void _toggleAthletics() {
    setState(() {
      runApp(AthleticsApp());
    });
  }

  void _toggleBlueJays() {
    setState(() {
      runApp(BlueJaysApp());
    });
  }

  void _toggleBraves() {
    setState(() {
      runApp(BravesApp());
    });
  }

  void _toggleBrewers() {
    setState(() {
      runApp(BrewersApp());
    });
  }

  void _toggleCardinals() {
    setState(() {
      runApp(CardinalsApp());
    });
  }

  void _toggleCubs() {
    setState(() {
      runApp(CubsApp());
    });
  }

  void _toggleDodgers() {
    setState(() {
      runApp(DodgersApp());
    });
  }

  void _toggleIndians() {
    setState(() {
      runApp(IndiansApp());
    });
  }

  void _toggleMarlins() {
    setState(() {
      runApp(MarlinsApp());
    });
  }

  void _toggleRays() {
    setState(() {
      runApp(RaysApp());
    });
  }

  void _toggleReds() {
    setState(() {
      runApp(RedsApp());
    });
  }

  void _toggleTwins() {
    setState(() {
      runApp(TwinsApp());
    });
  }

  void _toggleWhiteSox() {
    setState(() {
      runApp(WhiteSoxApp());
    });
  }

  void _toggleYankees() {
    setState(() {
      runApp(YankeesApp());
    });
  }
}

class PadresApp extends StatefulWidget {
  _PadresAppState createState() => _PadresAppState();
}

class _PadresAppState extends State<PadresApp> with SingleTickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(duration: const Duration(seconds: 2), vsync: this);
    animation = CurvedAnimation(parent: controller, curve: Curves.easeIn)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          controller.reverse();
        } else if (status == AnimationStatus.dismissed) {
          runApp(MyApp());
        }
      })
      ..addStatusListener((state) => print('$state'));
    controller.forward();
  }

  @override
  Widget build(BuildContext context) => AnimatedPadres(animation: animation);

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}

class AnimatedPadres extends AnimatedWidget {
  static final _opacityTween = Tween<double>(begin: 0.1, end: 1);
  static final _sizeTween = Tween<double>(begin: 0, end: 300);

  AnimatedPadres({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final animation = listenable as Animation<double>;
    return Center(
      child: Opacity(
        opacity: _opacityTween.evaluate(animation),
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 10),
          height: _sizeTween.evaluate(animation),
          width: _sizeTween.evaluate(animation),
          child: Image.asset('images/Padres.png'),
          //child: FlutterLogo(),
        ),
      ),
    );
  }
}

class AstrosApp extends StatefulWidget {
  _AstrosAppState createState() => _AstrosAppState();
}

class _AstrosAppState extends State<AstrosApp> with SingleTickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(duration: const Duration(seconds: 2), vsync: this);
    animation = CurvedAnimation(parent: controller, curve: Curves.easeIn)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          controller.reverse();
        } else if (status == AnimationStatus.dismissed) {
          runApp(MyApp());
        }
      })
      ..addStatusListener((state) => print('$state'));
    controller.forward();
  }

  @override
  Widget build(BuildContext context) => AnimatedAstros(animation: animation);

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}

class AnimatedAstros extends AnimatedWidget {
  static final _opacityTween = Tween<double>(begin: 0.1, end: 1);
  static final _sizeTween = Tween<double>(begin: 0, end: 300);

  AnimatedAstros({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final animation = listenable as Animation<double>;
    return Center(
      child: Opacity(
        opacity: _opacityTween.evaluate(animation),
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 10),
          height: _sizeTween.evaluate(animation),
          width: _sizeTween.evaluate(animation),
          child: Image.asset('images/Astros.png'),
          //child: FlutterLogo(),
        ),
      ),
    );
  }
}

class AthleticsApp extends StatefulWidget {
  _AthleticsAppState createState() => _AthleticsAppState();
}

class _AthleticsAppState extends State<AthleticsApp> with SingleTickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(duration: const Duration(seconds: 2), vsync: this);
    animation = CurvedAnimation(parent: controller, curve: Curves.easeIn)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          controller.reverse();
        } else if (status == AnimationStatus.dismissed) {
          runApp(MyApp());
        }
      })
      ..addStatusListener((state) => print('$state'));
    controller.forward();
  }

  @override
  Widget build(BuildContext context) => AnimatedAthletics(animation: animation);

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}

class AnimatedAthletics extends AnimatedWidget {
  static final _opacityTween = Tween<double>(begin: 0.1, end: 1);
  static final _sizeTween = Tween<double>(begin: 0, end: 300);

  AnimatedAthletics({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final animation = listenable as Animation<double>;
    return Center(
      child: Opacity(
        opacity: _opacityTween.evaluate(animation),
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 10),
          height: _sizeTween.evaluate(animation),
          width: _sizeTween.evaluate(animation),
          child: Image.asset('images/Athletics.png'),
          //child: FlutterLogo(),
        ),
      ),
    );
  }
}

class BlueJaysApp extends StatefulWidget {
  _BlueJaysAppState createState() => _BlueJaysAppState();
}

class _BlueJaysAppState extends State<BlueJaysApp> with SingleTickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(duration: const Duration(seconds: 2), vsync: this);
    animation = CurvedAnimation(parent: controller, curve: Curves.easeIn)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          controller.reverse();
        } else if (status == AnimationStatus.dismissed) {
          runApp(MyApp());
        }
      })
      ..addStatusListener((state) => print('$state'));
    controller.forward();
  }

  @override
  Widget build(BuildContext context) => AnimatedBlueJays(animation: animation);

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}

class AnimatedBlueJays extends AnimatedWidget {
  static final _opacityTween = Tween<double>(begin: 0.1, end: 1);
  static final _sizeTween = Tween<double>(begin: 0, end: 300);

  AnimatedBlueJays({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final animation = listenable as Animation<double>;
    return Center(
      child: Opacity(
        opacity: _opacityTween.evaluate(animation),
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 10),
          height: _sizeTween.evaluate(animation),
          width: _sizeTween.evaluate(animation),
          child: Image.asset('images/BlueJays.png'),
          //child: FlutterLogo(),
        ),
      ),
    );
  }
}

class BravesApp extends StatefulWidget {
  _BravesAppState createState() => _BravesAppState();
}

class _BravesAppState extends State<BravesApp> with SingleTickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(duration: const Duration(seconds: 2), vsync: this);
    animation = CurvedAnimation(parent: controller, curve: Curves.easeIn)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          controller.reverse();
        } else if (status == AnimationStatus.dismissed) {
          runApp(MyApp());
        }
      })
      ..addStatusListener((state) => print('$state'));
    controller.forward();
  }

  @override
  Widget build(BuildContext context) => AnimatedBraves(animation: animation);

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}

class AnimatedBraves extends AnimatedWidget {
  static final _opacityTween = Tween<double>(begin: 0.1, end: 1);
  static final _sizeTween = Tween<double>(begin: 0, end: 300);

  AnimatedBraves({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final animation = listenable as Animation<double>;
    return Center(
      child: Opacity(
        opacity: _opacityTween.evaluate(animation),
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 10),
          height: _sizeTween.evaluate(animation),
          width: _sizeTween.evaluate(animation),
          child: Image.asset('images/Braves.png'),
          //child: FlutterLogo(),
        ),
      ),
    );
  }
}

class BrewersApp extends StatefulWidget {
  _BrewersAppState createState() => _BrewersAppState();
}

class _BrewersAppState extends State<BrewersApp> with SingleTickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(duration: const Duration(seconds: 2), vsync: this);
    animation = CurvedAnimation(parent: controller, curve: Curves.easeIn)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          controller.reverse();
        } else if (status == AnimationStatus.dismissed) {
          runApp(MyApp());
        }
      })
      ..addStatusListener((state) => print('$state'));
    controller.forward();
  }

  @override
  Widget build(BuildContext context) => AnimatedBrewers(animation: animation);

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}

class AnimatedBrewers extends AnimatedWidget {
  static final _opacityTween = Tween<double>(begin: 0.1, end: 1);
  static final _sizeTween = Tween<double>(begin: 0, end: 300);

  AnimatedBrewers({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final animation = listenable as Animation<double>;
    return Center(
      child: Opacity(
        opacity: _opacityTween.evaluate(animation),
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 10),
          height: _sizeTween.evaluate(animation),
          width: _sizeTween.evaluate(animation),
          child: Image.asset('images/Brewers.png'),
          //child: FlutterLogo(),
        ),
      ),
    );
  }
}

class CardinalsApp extends StatefulWidget {
  _CardinalsAppState createState() => _CardinalsAppState();
}

class _CardinalsAppState extends State<CardinalsApp> with SingleTickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(duration: const Duration(seconds: 2), vsync: this);
    animation = CurvedAnimation(parent: controller, curve: Curves.easeIn)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          controller.reverse();
        } else if (status == AnimationStatus.dismissed) {
          runApp(MyApp());
        }
      })
      ..addStatusListener((state) => print('$state'));
    controller.forward();
  }

  @override
  Widget build(BuildContext context) => AnimatedCardinals(animation: animation);

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}

class AnimatedCardinals extends AnimatedWidget {
  static final _opacityTween = Tween<double>(begin: 0.1, end: 1);
  static final _sizeTween = Tween<double>(begin: 0, end: 300);

  AnimatedCardinals({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final animation = listenable as Animation<double>;
    return Center(
      child: Opacity(
        opacity: _opacityTween.evaluate(animation),
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 10),
          height: _sizeTween.evaluate(animation),
          width: _sizeTween.evaluate(animation),
          child: Image.asset('images/Cardinals.png'),
          //child: FlutterLogo(),
        ),
      ),
    );
  }
}

class CubsApp extends StatefulWidget {
  _CubsAppState createState() => _CubsAppState();
}

class _CubsAppState extends State<CubsApp> with SingleTickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(duration: const Duration(seconds: 2), vsync: this);
    animation = CurvedAnimation(parent: controller, curve: Curves.easeIn)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          controller.reverse();
        } else if (status == AnimationStatus.dismissed) {
          runApp(MyApp());
        }
      })
      ..addStatusListener((state) => print('$state'));
    controller.forward();
  }

  @override
  Widget build(BuildContext context) => AnimatedCubs(animation: animation);

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}

class AnimatedCubs extends AnimatedWidget {
  static final _opacityTween = Tween<double>(begin: 0.1, end: 1);
  static final _sizeTween = Tween<double>(begin: 0, end: 300);

  AnimatedCubs({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final animation = listenable as Animation<double>;
    return Center(
      child: Opacity(
        opacity: _opacityTween.evaluate(animation),
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 10),
          height: _sizeTween.evaluate(animation),
          width: _sizeTween.evaluate(animation),
          child: Image.asset('images/Cubs.png'),
          //child: FlutterLogo(),
        ),
      ),
    );
  }
}

class DodgersApp extends StatefulWidget {
  _DodgersAppState createState() => _DodgersAppState();
}

class _DodgersAppState extends State<DodgersApp> with SingleTickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(duration: const Duration(seconds: 2), vsync: this);
    animation = CurvedAnimation(parent: controller, curve: Curves.easeIn)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          controller.reverse();
        } else if (status == AnimationStatus.dismissed) {
          runApp(MyApp());
        }
      })
      ..addStatusListener((state) => print('$state'));
    controller.forward();
  }

  @override
  Widget build(BuildContext context) => AnimatedDodgers(animation: animation);

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}

class AnimatedDodgers extends AnimatedWidget {
  static final _opacityTween = Tween<double>(begin: 0.1, end: 1);
  static final _sizeTween = Tween<double>(begin: 0, end: 300);

  AnimatedDodgers({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final animation = listenable as Animation<double>;
    return Center(
      child: Opacity(
        opacity: _opacityTween.evaluate(animation),
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 10),
          height: _sizeTween.evaluate(animation),
          width: _sizeTween.evaluate(animation),
          child: Image.asset('images/Dodgers.png'),
          //child: FlutterLogo(),
        ),
      ),
    );
  }
}

class IndiansApp extends StatefulWidget {
  _IndiansAppState createState() => _IndiansAppState();
}

class _IndiansAppState extends State<IndiansApp> with SingleTickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(duration: const Duration(seconds: 2), vsync: this);
    animation = CurvedAnimation(parent: controller, curve: Curves.easeIn)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          controller.reverse();
        } else if (status == AnimationStatus.dismissed) {
          runApp(MyApp());
        }
      })
      ..addStatusListener((state) => print('$state'));
    controller.forward();
  }

  @override
  Widget build(BuildContext context) => AnimatedIndians(animation: animation);

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}

class AnimatedIndians extends AnimatedWidget {
  static final _opacityTween = Tween<double>(begin: 0.1, end: 1);
  static final _sizeTween = Tween<double>(begin: 0, end: 300);

  AnimatedIndians({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final animation = listenable as Animation<double>;
    return Center(
      child: Opacity(
        opacity: _opacityTween.evaluate(animation),
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 10),
          height: _sizeTween.evaluate(animation),
          width: _sizeTween.evaluate(animation),
          child: Image.asset('images/Indians.png'),
          //child: FlutterLogo(),
        ),
      ),
    );
  }
}

class MarlinsApp extends StatefulWidget {
  _MarlinsAppState createState() => _MarlinsAppState();
}

class _MarlinsAppState extends State<MarlinsApp> with SingleTickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(duration: const Duration(seconds: 2), vsync: this);
    animation = CurvedAnimation(parent: controller, curve: Curves.easeIn)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          controller.reverse();
        } else if (status == AnimationStatus.dismissed) {
          runApp(MyApp());
        }
      })
      ..addStatusListener((state) => print('$state'));
    controller.forward();
  }

  @override
  Widget build(BuildContext context) => AnimatedMarlins(animation: animation);

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}

class AnimatedMarlins extends AnimatedWidget {
  static final _opacityTween = Tween<double>(begin: 0.1, end: 1);
  static final _sizeTween = Tween<double>(begin: 0, end: 300);

  AnimatedMarlins({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final animation = listenable as Animation<double>;
    return Center(
      child: Opacity(
        opacity: _opacityTween.evaluate(animation),
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 10),
          height: _sizeTween.evaluate(animation),
          width: _sizeTween.evaluate(animation),
          child: Image.asset('images/Marlins.png'),
          //child: FlutterLogo(),
        ),
      ),
    );
  }
}

class RaysApp extends StatefulWidget {
  _RaysAppState createState() => _RaysAppState();
}

class _RaysAppState extends State<RaysApp> with SingleTickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(duration: const Duration(seconds: 2), vsync: this);
    animation = CurvedAnimation(parent: controller, curve: Curves.easeIn)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          controller.reverse();
        } else if (status == AnimationStatus.dismissed) {
          runApp(MyApp());
        }
      })
      ..addStatusListener((state) => print('$state'));
    controller.forward();
  }

  @override
  Widget build(BuildContext context) => AnimatedRays(animation: animation);

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}

class AnimatedRays extends AnimatedWidget {
  static final _opacityTween = Tween<double>(begin: 0.1, end: 1);
  static final _sizeTween = Tween<double>(begin: 0, end: 300);

  AnimatedRays({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final animation = listenable as Animation<double>;
    return Center(
      child: Opacity(
        opacity: _opacityTween.evaluate(animation),
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 10),
          height: _sizeTween.evaluate(animation),
          width: _sizeTween.evaluate(animation),
          child: Image.asset('images/Rays.png'),
          //child: FlutterLogo(),
        ),
      ),
    );
  }
}

class RedsApp extends StatefulWidget {
  _RedsAppState createState() => _RedsAppState();
}

class _RedsAppState extends State<RedsApp> with SingleTickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(duration: const Duration(seconds: 2), vsync: this);
    animation = CurvedAnimation(parent: controller, curve: Curves.easeIn)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          controller.reverse();
        } else if (status == AnimationStatus.dismissed) {
          runApp(MyApp());
        }
      })
      ..addStatusListener((state) => print('$state'));
    controller.forward();
  }

  @override
  Widget build(BuildContext context) => AnimatedReds(animation: animation);

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}

class AnimatedReds extends AnimatedWidget {
  static final _opacityTween = Tween<double>(begin: 0.1, end: 1);
  static final _sizeTween = Tween<double>(begin: 0, end: 300);

  AnimatedReds({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final animation = listenable as Animation<double>;
    return Center(
      child: Opacity(
        opacity: _opacityTween.evaluate(animation),
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 10),
          height: _sizeTween.evaluate(animation),
          width: _sizeTween.evaluate(animation),
          child: Image.asset('images/Reds.png'),
          //child: FlutterLogo(),
        ),
      ),
    );
  }
}

class TwinsApp extends StatefulWidget {
  _TwinsAppState createState() => _TwinsAppState();
}

class _TwinsAppState extends State<TwinsApp> with SingleTickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(duration: const Duration(seconds: 2), vsync: this);
    animation = CurvedAnimation(parent: controller, curve: Curves.easeIn)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          controller.reverse();
        } else if (status == AnimationStatus.dismissed) {
          runApp(MyApp());
        }
      })
      ..addStatusListener((state) => print('$state'));
    controller.forward();
  }

  @override
  Widget build(BuildContext context) => AnimatedTwins(animation: animation);

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}

class AnimatedTwins extends AnimatedWidget {
  static final _opacityTween = Tween<double>(begin: 0.1, end: 1);
  static final _sizeTween = Tween<double>(begin: 0, end: 300);

  AnimatedTwins({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final animation = listenable as Animation<double>;
    return Center(
      child: Opacity(
        opacity: _opacityTween.evaluate(animation),
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 10),
          height: _sizeTween.evaluate(animation),
          width: _sizeTween.evaluate(animation),
          child: Image.asset('images/Twins.png'),
          //child: FlutterLogo(),
        ),
      ),
    );
  }
}

class WhiteSoxApp extends StatefulWidget {
  _WhiteSoxAppState createState() => _WhiteSoxAppState();
}

class _WhiteSoxAppState extends State<WhiteSoxApp> with SingleTickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(duration: const Duration(seconds: 2), vsync: this);
    animation = CurvedAnimation(parent: controller, curve: Curves.easeIn)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          controller.reverse();
        } else if (status == AnimationStatus.dismissed) {
          runApp(MyApp());
        }
      })
      ..addStatusListener((state) => print('$state'));
    controller.forward();
  }

  @override
  Widget build(BuildContext context) => AnimatedWhiteSox(animation: animation);

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}

class AnimatedWhiteSox extends AnimatedWidget {
  static final _opacityTween = Tween<double>(begin: 0.1, end: 1);
  static final _sizeTween = Tween<double>(begin: 0, end: 300);

  AnimatedWhiteSox({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final animation = listenable as Animation<double>;
    return Center(
      child: Opacity(
        opacity: _opacityTween.evaluate(animation),
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 10),
          height: _sizeTween.evaluate(animation),
          width: _sizeTween.evaluate(animation),
          child: Image.asset('images/WhiteSox.png'),
          //child: FlutterLogo(),
        ),
      ),
    );
  }
}

class YankeesApp extends StatefulWidget {
  _YankeesAppState createState() => _YankeesAppState();
}

class _YankeesAppState extends State<YankeesApp> with SingleTickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(duration: const Duration(seconds: 2), vsync: this);
    animation = CurvedAnimation(parent: controller, curve: Curves.easeIn)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          controller.reverse();
        } else if (status == AnimationStatus.dismissed) {
          runApp(MyApp());
        }
      })
      ..addStatusListener((state) => print('$state'));
    controller.forward();
  }

  @override
  Widget build(BuildContext context) => AnimatedYankees(animation: animation);

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}

class AnimatedYankees extends AnimatedWidget {
  static final _opacityTween = Tween<double>(begin: 0.1, end: 1);
  static final _sizeTween = Tween<double>(begin: 0, end: 300);

  AnimatedYankees({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final animation = listenable as Animation<double>;
    return Center(
      child: Opacity(
        opacity: _opacityTween.evaluate(animation),
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 10),
          height: _sizeTween.evaluate(animation),
          width: _sizeTween.evaluate(animation),
          child: Image.asset('images/Yankees.png'),
          //child: FlutterLogo(),
        ),
      ),
    );
  }
}